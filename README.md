# Project name: eks-terraform

# objectives
This project is for create an kubernetes cluster with EKS and terraform.

# requisites
- cluster with terraform
- cluster with eks

# to do
- install certmanager
- install nginx ingress controller

# doing
- create an terraform to deploy the environment
- connect to cluster with eksctl

# done
- configure the aws cli
- test the aws cli

# improvements
- 

# references
- https://learn.hashicorp.com/tutorials/terraform/eks
